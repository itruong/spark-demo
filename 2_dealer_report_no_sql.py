import os
import sys

# hackety way to make Pycharm not complain about pyspark reference
# source: http://renien.github.io/blog/accessing-pyspark-pycharm/
os.environ['SPARK_HOME'] = '/usr/local/spark-1.2.0'
sys.path.append('/usr/local/spark-1.2.0/python')

try:
    from pyspark import SparkConf, SparkContext
except ImportError as e:
    print ('Unable to import Spark Modules', e)
    sys.exit(1)

# setting spark.hadoop.validateOutputSpecs to false to allow overwriting of
# the result set in hdfs storage
conf = (SparkConf()
        .setMaster('local[4]')
        .set('spark.eventLog.enabled', 'true')
        .set('spark.hadoop.validateOutputSpecs', 'false'))

sc = SparkContext(conf=conf)
di = sc.textFile('file:///code/data/dealer_info.txt')\
    .map(lambda l: l.split('\t'))\
    .cache()

m = sc.textFile('file:///code/data/makes.txt')\
    .map(lambda l: l.split('\t'))\
    .cache()

dc = sc.textFile('file:///code/data/dealer_config.txt')\
    .map(lambda l: l.split('\t'))\
    .cache()

dealer_info = di.map(lambda l: (l[0], {
    'dealer_id': l[0],
    'cars_dealer_id': l[17],
    'name': l[1],
    'city_location': l[8],
    'state_location': l[9],
    'zip_location': l[10]}))

dealer_config = dc.map(
    lambda l: (l[0], {
        'dealer_id': l[0],
        'make_id': l[6],
        'active': l[28]}))\
    .filter(lambda l: l[1]['active'] == '2')

makes = m.map(lambda l: (l[0], l[1])).collectAsMap()

dj = dealer_config.join(dealer_info)\
    .sortByKey()\
    .map(lambda t: {
        'dealer_id': t[1][1]['dealer_id'],
        'cars_dealer_id': t[1][1]['cars_dealer_id'],
        'name': t[1][1]['name'],
        'city_location': t[1][1]['city_location'],
        'state_location': t[1][1]['state_location'],
        'zip_location': t[1][1]['zip_location'],
        'make_id': t[1][0]['make_id'],
        'active': t[1][0]['active']
})

result = dj.map(lambda r: u'{0},{1},{2},{3},{4},{5},{6}'
                .format(r['dealer_id'], r['cars_dealer_id'], r['name'],
                        makes[r['make_id']], r['city_location'],
                        r['state_location'], r['zip_location']))

result.saveAsTextFile('active_dealers')
