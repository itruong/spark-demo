import os
import sys

# hackety way to make Pycharm not complain about pyspark reference
# source: http://renien.github.io/blog/accessing-pyspark-pycharm/
os.environ['SPARK_HOME'] = '/usr/local/spark-1.2.0'
sys.path.append('/usr/local/spark-1.2.0/python')

try:
    from pyspark import SparkConf, SparkContext
    from pyspark.sql import SQLContext, Row
except ImportError as e:
    print ('Unable to import Spark Modules', e)
    sys.exit(1)

# setting spark.hadoop.validateOutputSpecs to false to allow overwriting of
# the result set in hdfs storage
conf = (SparkConf()
        .setMaster('local[4]')
        .set('spark.eventLog.enabled', 'true')
        .set('spark.hadoop.validateOutputSpecs', 'false'))

sc = SparkContext(conf=conf)
di = sc.textFile('file:///code/data/dealer_info.txt')\
    .map(lambda l: l.split('\t'))\
    .cache()

m = sc.textFile('file:///code/data/makes.txt')\
    .map(lambda l: l.split('\t'))\
    .cache()

dc = sc.textFile('file:///code/data/dealer_config.txt')\
    .map(lambda l: l.split('\t'))\
    .cache()

sq = SQLContext(sc)

dealer_info = di.map(lambda l: Row(
    dealer_id=l[0],
    cars_dealer_id=l[17],
    name=l[1],
    city_location=l[8],
    state_location=l[9],
    zip_location=l[10]))

dealer_config = dc.map(lambda l: Row(
    dealer_id=l[0],
    make_id=l[6],
    active=l[28]))

makes = m.map(lambda l: Row(
    make_id=l[0],
    make_name=l[1]))

schema_di = sq.inferSchema(dealer_info)
schema_dc = sq.inferSchema(dealer_config)
schema_m = sq.inferSchema(makes)

schema_di.registerTempTable('dealer_info')
schema_dc.registerTempTable('dealer_config')
schema_m.registerTempTable('makes')

rows = sq.sql('select dc.dealer_id, di.cars_dealer_id, di.name, m.make_name, '
              'di.city_location, di.state_location, di.zip_location '
              'from dealer_config dc '
              'join dealer_info di on di.dealer_id = dc.dealer_id '
              'join makes m on m.make_id = dc.make_id '
              'where dc.active = 2 order by dc.dealer_id, di.cars_dealer_id')

result = rows.map(lambda r: '{0},{1},{2},{3},{4},{5},{6}'
                  .format(r.dealer_id, r.cars_dealer_id, r.name,
                          r.make_name, r.city_location, r.state_location,
                          r.zip_location))

result.saveAsTextFile('active_dealers')
